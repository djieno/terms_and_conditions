# `Terms and Conditions` _may_ apply ;-)

Freelancers, SysOps people, Devops practitioners, IT crowd people. We tend to attract recruiters. In some cases a lot of recruiters.

This is not a bad thing! In fact I would say we are in a very luxurious position if we attract so much attention right?

Unfortunately, there are a few recruiters that still maintain a somewhat antiquated `Terms and Conditions` system for our future partnership.

For instance they would like you to keep your hours up, you cannot simply take a day off without contacting them in advance. Or they expect you to be at their "team days" where you get to meet the other contractors they placed with other companies. (Why? I do not know these people?).

So in an effort to try and make relations easier, and keep things clear, honest and transparent from the get-go, I made a few `Terms and Conditions` of myself. 

They are made with a giggle, but also have a serious undertone.

## 1. No Word format CV
Nope, sorry; I do not have MS office on my machine(s) and exporting from LibreOffice always looks awful. You would not want to have that anyway!

Plus, My CV is online. It's partly (well mostly) on Linked-in, some of it is in GitHub, Blog and/or forum posts, etc..

Paper or single document type CV's, just like the fax machine, no longer have the same impact as it once had.

The most I can do is a pretty CV in PDF file-type, based on my Linked-in page. ([Ceev.io](https://ceev.io) is awesome)

## 2. Stepping out
It must be possible for the recruiting party to step away between the customer and me; the contractor.

For instance: Let's say you want to add a fee of €10 per hour to my hourly rate. After 6 months of ~168 hours per month you will have made ~ €10000. Deduct some taxes and costs; and it comes down to ~ €7000. That's a good sum to have made.

And some recruiters add €15 per hour.

After those 6 months, the recruiter will step out, say goodbye and let the customer and me (the contractor) divide that €10. **Everybody** happy.

## 3. No BBQ.
Some recruiters will try and persuade me into thinking they are not your average recruiter! They will arrange funky BBQ's and knowledge sharing sessions. Well unless I can bill those hours to you; the recruiter, I see no reason to freely share my knowledge. Unless of course when we talk about meetups.

## 4. Hours! Billing!

### 4.1 You want to take the reigns?
I'm fine with registering my hours in your system, and you doing the "reverse billing" thing. Just know that I will keep my shadow registration as well, and when you loose my hours in your system (MySQL died!) then I send you a copy of my registration so you can type them in. Deal?

### 4.2 Other option; I do it for you
The other option: I have a beautiful system. I truly do. I document my hours, I get them signed, I create an invoice based on those hours. I send you the invoice with a copy of the signed paper for the hours. And then, within the payment term (14 days), you pay me! It's wonderful!

I send you invoices, based on that I would imagine you will want to invoice the customer; so I will be very specific on my invoices.

### 4.3 Hours?
Yeah about those, you see I partly became a freelancer because of the freedom and not just freedom of where to work but also how much I work. So if I take a day off, (In agreement with customer, OF COURSE) please do not make me feel guilty about it? Pretty please? I've had someone say to me: "But we budgeted those hours and now we are 16 hours short!..." 


----

If you are OK with the above (1 is non-negotiable, and 2 only on the "when it will happen") give me a call or email me!
